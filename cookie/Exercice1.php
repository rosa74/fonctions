<!DOCTYPE html>
<html lang='fr'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>

<body>

    <?php

    // Introduction aux formulaires
    // Pour autoriser l'accès à un utilisateur, son adresse IP doit être 192.168.22.77 ET il doit avoir au moins 15 ans; 
    // Ou alors, son adresse IP  et son age peut être n'importe lesquels mais il doit être abonné.
    // Suivant les valeurs renvoyées par la fonction, affichez un message à l'utilisateur qui doit être soit: 
    // Bonjour visiteur, vous allez être redirigé vers la page d'inscription
    // ou 
    // Bonjour, content de vous revoir Paul
    // ( Utilisez TRUE et FALSE )


    //faites un formulaire
    // si le nom envoye est Pierre,et que son ip est 77 et qu 'il a plus de 15 ans
    //c'est bon, renvoyer son pseudo qui est :codeur 
    // sinon rediriger le vers la page 'inscription

    ?>

    <!-- écrire le code après ce commentaire -->

    <form method="get">
        <input type="text" name="prenom">
        <input type="submit" value="Envoyer">
    </form>
    <!-- écrire le code après ce commentaire -->

    <?php

    $randip = rand(75, 77);
    $ip = '192.168.22.' . $randip;
    $age = rand(14, 16);

    $prenom = isset($_GET['prenom']) ? $_GET['prenom'] : '';

    // $demandeDeConnexion = ['Inconnu' => 'Pierre', 'Abonné' => 'Paul'];

    function Auth($ip, $age, $prenom)
    {


        if ($ip === '192.168.22.77' && $age >= 15 && $prenom == 'Pierre') {
            $acces = TRUE;
            $pseudo = 'Codeur';
        } else {
            $acces = FALSE;
            $pseudo = null;
        }

        return [$acces, $pseudo];
    }

    $result = Auth($ip, $age, $prenom);

    if ($result[0] == FALSE) {
        echo 'Bonjour visiteur, vous allez être redirigé vers la page d\'inscription';
    } else {
        echo 'Bonjour, content de vous revoir ' . $result[1];
    }



    ?>

    <!-- écrire le code avant ce commentaire -->

</body>

</html>


<!-- écrire le code avant ce commentaire -->

</body>

</html>