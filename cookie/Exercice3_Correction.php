<?php
session_start();
?><!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
    <style>
    form{
        width: 40%;
        text-align: center;
        margin: 0 auto;
        border: none;
        border-radius: 15px;
        box-shadow: 0 0 7px grey;
        display:flex;
        flex-direction:column;
        padding:30px; 

    }

    input[type=text] {
        padding: 10px;
        width: 90%;
        border: none;
        border-radius: 5px;
        box-shadow: 0 0 5px grey;
        font-size: 20px;
        margin: 10px auto;
    }

    input[type=submit] {
        padding: 10px;
        background-color: royalblue;
        color: white;
        width: 90%;
        border: none;
        border-radius: 5px;
        box-shadow: 0 0 5px grey;
        font-size: 20px;
        margin: 0 auto;
    }

    .valid {
        width:40%;
        padding:10px;
        font-familly:Arial;
        font-weight:bold;
        background-color: rgba(24, 209, 0, 0.616);
        margin: 20px auto;
        border-radius: 5px;
        text-align:center;
    }

    .error {
        width:40%;
        padding:10px;
        font-familly:Arial;
        font-weight:bold;
        background-color: rgba(209, 59, 0, 0.616);
        margin: 20px auto;
        border-radius: 5px;
        text-align:center;
    }
.btn{
    text-decoration : none;
    background:blue;
    color:white;
    font-familly:Arial;
    padding:5px;

}
    </style>
</head>
    <body>
            
        <?php
    
             // Contrôle de formulaire
             // Créer une page avec un formulaire qui demande : 
             // - Le pseudo (maxi 255 caractères)
             // - un Mail (qui doit être valide; cf : filter_var)
             // - La confirmation du mail 
             // - Le mot de passe
             // - La confirmation du mot de passe
             // Créer une fonction qui vérifie que tous les champs soient valides, 
             // ou renvoyer une erreur en fonction de l'erreur rencontrée, seulement si les champs sont envoyés et qu'ils sont remplis
             // Créez une variable de session avec le pseudo 
             // Si l'utilisateur est validé, proposer lui, avec un bouton, d'aller sur son espace personnel, où sont pseudo sera affichez

            
        ?>
        
            
        <!-- écrire le code après ce commentaire -->

                <?php
                $error = null;
                
                if(isset($_POST['pseudo']) && isset($_POST['mail']) && isset($_POST['mail2']) && isset($_POST['mdp']) && isset($_POST['mdp2'])){
                    if(!empty($_POST['pseudo']) && !empty($_POST['mail']) && !empty($_POST['mail2']) && !empty($_POST['mdp']) && !empty($_POST['mdp2'])){
                        $pseudo = $_POST['pseudo'];
                        $mail = $_POST['mail'];
                        $mail2 = $_POST['mail2'];
                        $mdp = $_POST['mdp'];
                        $mdp2 = $_POST['mdp2'];

                        function formulaire($pseudo, $mail, $mail2, $mdp, $mdp2){
                        $lengt = iconv_strlen($pseudo); // Se code pouvait être reduit en mettant un maxlength="255" dans l'input du pseudo 
                        if($lengt <= 255){
                            if (filter_var($mail, FILTER_VALIDATE_EMAIL)){ // Se code pouvait être réduit en faisant un input de type email
                                if($mail == $mail2){
                                    if(ctype_alpha($mdp) == true){
                                        if($mdp == $mdp2){

                                                if($pseudo == 'Polo' && $mdp == 'enssop' ){
                                                    $_SESSION['pseudo'] = $pseudo;
                                                    $error = "<p class='valid'>Bonjour ".$pseudo." votre accès est autorisé <br><br> <a class='btn' href='Exercice3-2.php' >Allez sur mon espace</a></p>"; 
                                                } else {
                                                    $error = '<p class="error">Désolé '.$pseudo.' votre accès est refusé</p>';
                                                }

                                            } else {
                                            $error = '<p class="error">Vos mots de passes ne sont pas identiques</p>';
                                        }
                                    } else{
                                        $error = '<p class="error">Votre mot de passe doit contenir que des lettres</p>';
                                    } 

                                } else {
                                    $error = '<p class="error">Les deux mails ne sont pas identiques</p>';
                                }
                            } else {
                                $error = '<p class="error">Votre mail n\'est pas valide</p>';
                            }
                        } else {
                            $error = '<p class="error">Votre pseudo est trop long</p>';
                        }
                            return $error;
                      }

                      $error = formulaire($pseudo, $mail, $mail2, $mdp, $mdp2);
                      
                    }
                } 

                echo $error;
                ?>
            
            
            <form action="Exercice3-1.php" method="POST">
                <h2>Connexion</h2>
                <input type="text" name="pseudo" placeholder="Votre pseudo">
                <input type="text" name="mail" placeholder="Votre mail">
                <input type="text" name="mail2" placeholder="Confirmer votre mail">
                <input type="text" name="mdp" placeholder="Votre mot de passe">
                <input type="text" name="mdp2" placeholder="Confirmer votre mot de passe">
                <input type="submit" value="Se connecter">
            </form>

            <!-- *********************************
            
            Dans la seconde page, il y a simplement ceci : 
                    
                <?php 
                session_start();
                
                echo '<p class="valid">Bonjour '.$_SESSION['pseudo'].'</p>'; 
                
                ?>
                
             ********************************* -->

        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>