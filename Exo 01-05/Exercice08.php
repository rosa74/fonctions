<!DOCTYPE html>
<html lang='fr'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>

<body>

    <?php

    // Faites un formulaire afin de récuperer la largeur, la longeur et le nombre de niveaux d'un maison
    // Faites une fonction afin de calculer la surface de la maison
    // A l'exterieur de la fonction, multiplier cette valeur par 10 afin de connaitre la taille d'un futur immeuble

    ?>

    <!-- écrire le code après ce commentaire -->

    <form method="get">

        <div>
            <label for="number" name="surface" value="<?php if (!empty($_GET['surface'])) echo $_GET['surface'] ?>"> Calcul de la Surface</label>
        </div>

        <div>
            <input type="text"  name="largeur" >Largeur</input>
        </div>

        <div>
            <input type="text" name="longueur" >Longeur</input>
        </div>

        <div>
            <input type="text" name="niveau" >Nombre de niveau dans la maison</input>
        </div>

        <input type="submit" name='calcul'>
    </form>



    <?php


    $largeur = isset($_GET['largeur']) ? intval($_GET['largeur']) : 0;
    $longueur = isset($_GET['longueur']) ? intval($_GET['longueur']) : 0;
    $niveau = isset($_GET['niveau']) ? intval($_GET['niveau']) : 0;

    function superficie($l, $long, $niv)
    {

        $surface = ($l * $long) * $niv;

        return $surface;
    }
    echo 'La surface de la maison est de ' . superficie($largeur, $longueur, $niveau) . 'm²';
    ?>

</body>

</html>