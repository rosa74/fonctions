<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
    <body>
            
        <?php
    
             // Calculez le coût de la recharge pour une voiture éléctrique en fonction du nombre de Kw qu'il reste
             // La batterie pleine contient 41Kw maximum. La forme du résultat est la suivante :
             //
             // Il reste dans la voiture 22Kw sur 41 avant la recharge,
             // Le prix du Kw étant à 0.13 €/Kw à domicile,
             // lorsque je vais recharger les 19Kw manquant,
             // cela va me couter 2.47 €
            
        ?>
            
        <!-- écrire le code après ce commentaire -->
            
                <?php
                
                    $BatterieRestante = rand(1,41); // En Kw
                    $prixDuKw = 0.13;

                    echo 'Il reste dans la voiture ' .$BatterieRestante. ' Kw sur 41 avant la recharge <br>';
                    echo 'Le prix du Kw étant à ' .$prixDuKw. ' €/Kw à domicile <br>';

                    function recharge($i,$j)
                    {
                    
                    $resultat=41-$i;
                    $cout=$resultat*$j;

                    return [$resultat,$cout];

                    }

                    $x=recharge($BatterieRestante,$prixDuKw);

                    
                    echo 'lorsque je vais recharger les '.$x[0].'kw manquant <br>';
                    echo 'cela va me couter '.$x[1].' € <br>';

 
                ?>
            
        <!-- écrire le code avant ce commentaire -->
        
    </body>
</html>