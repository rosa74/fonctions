<!DOCTYPE html>
<html lang='fr'>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>

</head>

<body>

    <?php

    // Mélangeur de couleur
    // Faites un formulaire qui prends en compte les 3 couleurs primaires : Rouge Bleu Jaune
    // Si l'utilisateur sélectionne le bleu et le jaune, le formulaire doit lui renvoyer du vert
    // Les couleurs seront matérialisées par des ronds avec la couleur correspondante
    // Le code php complet (correctement indentée) doit faire maximum 30 lignes (ou 24)

    ?>
    <form method="post">

        <div style='text-align:center'>

            <div style="display:flex;justify-content:space-around;margin:20px 20%">
                <div style="height:100px;width:100px;border-radius:50%;background:red"></div>
                <div style="height:100px;width:100px;border-radius:50%;background:blue"></div>
                <div style="height:100px;width:100px;border-radius:50%;background:yellow"></div>
            </div>

            <div style="display:flex;justify-content:space-around;margin:20px 20%">
                <input type="checkbox" name="rouge" value="Rouge">
                <input type="checkbox" name="bleu" value="Bleu">
                <input type="checkbox" name="jaune" value="Jaune">
            </div>
            
                <input style="background-color:grey;padding:20px;" type='submit' value='Voir le resultat du mélange'>
        </div>
    </form>

    </form>

    <!-- écrire le code après ce commentaire -->
    <?php

    $recup = isset($_POST['rouge']);
    $recup1 = isset($_POST['bleu']);
    $recup2 = isset($_POST['jaune']);

    if ($recup == 'on' || $recup1 == 'on' || $recup2 == 'on' ){

    function choixcouleur($a, $b, $c)
    {
        if ($a == 'on' && $b == 'on' && $c == 'on') {
            echo '<div style="display:flex;justify-content:space-around;align-items:center;padding:20px;"><div style="height:100px;width:100px;border-radius:50%;background:red"></div> + <div style="height:100px;width:100px;border-radius:50%;background:blue"></div>  + <div style="height:100px;width:100px;border-radius:50%;background:yellow"></div>  = <div style="height:100px;width:100px;border-radius:50%;background:black"></div></div>';
        } elseif ($a == 'on' && $b == 'on' && $c != 'on') {
            echo '<div style="display:flex;justify-content:space-around;align-items:center;padding:20px;"><div style="height:100px;width:100px;border-radius:50%;background:red"></div> + <div style="height:100px;width:100px;border-radius:50%;background:blue"></div>= <div style="height:100px;width:100px;border-radius:50%;background:purple"></div>';
        } elseif ($a == 'on' && $b != 'on' && $c == 'on') {
            echo '<div style="display:flex;justify-content:space-around;align-items:center;padding:20px;"><div style="height:100px;width:100px;border-radius:50%;background:red"></div>  + <div style="height:100px;width:100px;border-radius:50%;background:yellow"></div>  = <div style="height:100px;width:100px;border-radius:50%;background:orange"></div>';
        } elseif ($a = 'on' && $b == 'on' && $c == 'on') {
            echo '<div style="display:flex;justify-content:space-around;align-items:center;padding:20px;"><div style="height:100px;width:100px;border-radius:50%;background:blue"></div>  + <div style="height:100px;width:100px;border-radius:50%;background:yellow"></div>  = <div style="height:100px;width:100px;border-radius:50%;background:green"></div>';
        } else {
            echo '<div style="display:flex;justify-content:space-around;align-items:center;padding:20px;"><h1> "Choisissez aux moins deux couleurs " </h1></div>';
        }
    }

    choixcouleur($recup, $recup1, $recup2);
 }
    ?>


    <!-- écrire le code avant ce commentaire -->

</body>

</html>